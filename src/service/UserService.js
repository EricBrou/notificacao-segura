import Http from '../config/Http';

const login = async (userData) => {
    return(
        Http({
            method: 'POST',
            url: `/auth/login`,
            data: userData,
        }).then((response) => {
            return response.data.access_token;
        })
    );
}

const create = async (userData) => {
    return(
        Http({
            method: 'POST',
            url: `/user/create`,
            data: userData,
        }).then((response) => {
            return response;
        })
    );
}

export default {
    login,
    create,
}