import Http from '../config/Http';

const create = async (notificationData, userToken) => {
    
    if(userToken){
        Http.defaults.headers.common['Authorization'] = `Bearer ${userToken}`
    }
    
    return(
        Http({
            method: 'POST',
            url: `/notification/create`,
            data: notificationData,
        }).then((response) => {
            return response;
        })
    );
}

const list = async (userToken) => {
    
    if(userToken){
        Http.defaults.headers.common['Authorization'] = `Bearer ${userToken}`
    }
    
    return(
        Http({
            method: 'GET',
            url: `/notification/list`,
        }).then((response) => {
            return response?.data;
        })
    );
}

const edit = async (notificationData, userToken, id) => {
    
    if(userToken){
        Http.defaults.headers.common['Authorization'] = `Bearer ${userToken}`
    }
    
    return(
        Http({
            method: 'PUT',
            url: `/notification/edit/${id}`,
            data: notificationData,
        }).then((response) => {
            return response;
        })
    );
}

const exclude = async (userToken, id) => {
    
    if(userToken){
        Http.defaults.headers.common['Authorization'] = `Bearer ${userToken}`
    }
    
    return(
        Http({
            method: 'DELETE',
            url: `/notification/delete/${id}`,
        }).then((response) => {
            return response;
        })
    );
}