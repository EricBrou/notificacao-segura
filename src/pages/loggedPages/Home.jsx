import React, { useState } from 'react'
import { Text, View, Image, ScrollView } from 'react-native';
import GlobalStyle from '../../assets/styles/GlobalStyles';
import HStyle from '../loggedPages/HStyle';
import HomeIcon from '../../assets/Icons/HomeIcon';
import { FlashList } from '@shopify/flash-list';
import TrashIcon from '../../assets/Icons/TrashIcon';
import UpdateIcon from '../../assets/Icons/UpdateIcon';

function Home() {

  const [dataNotification, setDataNotification] = useState([{titulo: "First Item", subtitulo: "Olá", descricao: "🎁 Não perca as ofertas incríveis no AliExpress! Explore uma seleção e..."}]);

  return (
    <>
      { dataNotification === '' ? (
        <View style={GlobalStyle.Container}>
          <Image style={HStyle.DefaultImage} source={require('../../assets/img/homeImage.png')}/>
          <Text style={HStyle.ContentEmpty}>Ainda não há nenhum Evento, crie um para começarmos!</Text>
          <HomeIcon />
          
        </View>
      ) : (
        <ScrollView contentContainerStyle={HStyle.ScrollViewHome}>
          <FlashList
              contentContainerStyle={HStyle.FlashListHome}
              disableAutoLayout={false}
              data={dataNotification}
              renderItem={({ item }) => 
                <View style={HStyle.ContainerListHome}>
                  <View style={HStyle.ContainerContentHome}>
                    <Text style={HStyle.TitleListHome}>{item.titulo}</Text>
                    <Text style={HStyle.SubtitleListHome}>{item.subtitulo}</Text>
                    <Text style={HStyle.DescriptionListHome}>{item.descricao}</Text>
                  </View>
                  <View style={HStyle.IconsContainer}>
                    <TrashIcon />
                    <UpdateIcon />
                  </View>
                  <View>
                    
                  </View>
                </View>
              }
              estimatedItemSize={200}
          />
        </ScrollView>
      )}
    </> 
  )

}

export default Home;