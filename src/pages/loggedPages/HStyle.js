import { StyleSheet } from 'react-native';

const HStyle = StyleSheet.create({

    //Estilização da Page Empty

    ContainerEmpty: {
        flexGrow: 1,
        alignItems: 'center',
        justifyContent: 'center',
      },

      ContentEmpty: {
        fontSize: 20,
        margin: 20,
        textAlign: 'center',
        color: '#686868'
    },

    ScrollViewHome: {
        flexGrow: 1,
        justifyContent: 'center',
        backgroundColor: 'white',
    },

    FlashListHome: {
        backgroundColor: 'white',
        padding: 10,
    },

    ContainerListHome: {
        display: 'flex',
        borderRadius: 20,
        padding: 20,
        shadowColor: '#000000',
        shadowOffset: {width: -2, height: 4},
        shadowOpacity: 0.2,
        shadowRadius: 3,
        elevation: 5,
        backgroundColor: 'white',
        maxHeight: 200,
    },

    ContainerContentHome: {
        width: '85%',
    },

    TitleListHome: {
        color: '#3C9F9F',
        fontWeight: '700',
        fontSize: 24,
        overflow: 'hidden',
    },

    SubtitleListHome: {
        fontSize: 22,
        fontWeight: '400',
        overflow: 'hidden',
    },

    DescriptionListHome: {
        color: '#686868',
        fontWeight: '300',
        fontSize: 20,
        overflow: 'hidden',
    },

    IconsContainer: {
        display: 'flex',
    },

    // Estilos Globais

    DefaultImage: {
        width: 250,
        height: 250,
        opacity: 0.5
    },

});

export default HStyle;