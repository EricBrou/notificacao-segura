import { StyleSheet } from 'react-native';

const RStyle = StyleSheet.create({

  // Estilização da Page Start:

  ContainerStart: {
    flexGrow: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },

  Title: {
    fontFamily: 'Roboto',
    fontSize: 25,
    margin: 20,
    fontWeight: 'bold',
    color: 'white',
    textAlign: 'center',
  },

  Content: {
    fontFamily: 'Roboto',
    fontSize: 18,
    textAlign: 'center',
    fontWeight: 'regular',
    color: 'white',
    marginLeft: 15,
    marginRight: 15,
  },

  DefaultImage: {
    width: 250,
    height: 250,
    borderRadius: 125,
  },

  // Estilização da Page Login:

  ForgotPassword: {
    fontWeight: 'regular',
    fontSize: 16,
    width: '70%',
    alignContent: 'flex-end',
  },

  ForgotPasswordText: {
    color: '#3C9F9F',
    fontSize: 18,
    textAlign: 'right',
  },

  DefaultIcon: {
    margin: 30,
  },

  // Estilização da Page Register: 

  TitleContainer: { //Empurrando os input contra o flex-grow do TabButton
    display: 'flex',
    flex: 1,
    justifyContent: 'center',
  },

  // Estilos Globais

  TabButtonContainer: { // Configurando botão Criar Conta/Realizar Login
    display: 'flex',
    flex: 1,
    justifyContent: 'flex-end',
    width: '100%',
  },

  TabButton: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    height: 70,
    backgroundColor: '#EDECEC'
  },

  TabButtonTitle: {
    fontSize: 20,
    fontWeight: 'bold',
    textTransform: 'uppercase',
    color: '#3C9F9F'
  },

  TitlePage: {  // Título da página "Login" e "Criar Conta"
    fontWeight: 'bold',
    fontSize: 28,
    width: '70%'
  },

  InputStyle: { // Estilizando os inputs
    margin: 10,
    height: 55,
    fontSize: 20,
    borderRadius: 8,
    backgroundColor: '#EDECEC',
    paddingLeft: 20,
    width: '75%',
  },
  
});


export default RStyle;