import React, { useState } from 'react'
import { View, Text, Pressable, TextInput, ScrollView } from 'react-native';
import Button from '../../components/Button';
import RStyle from './RStyle';
import GlobalStyles from '../../assets/styles/GlobalStyles';

function Register({ navigation }) {

  const [userData, setUserData] = useState({
    nome: '',
    email: '',
    telefone: '',
    senha: '',
    confirmarSenha: '',
  }); 

  const handleInputChange = (e) => {
      const { name, value } = e.target;
      setUserData({ ...userData, [name]: value });
  };

  const validateLogin = async () => {
  
  }

  return (
    <ScrollView contentContainerStyle={GlobalStyles.ContainerScrollView}>
      <View style={GlobalStyles.Container}>

        <View style={RStyle.TitleContainer}>
          <Text style={RStyle.TitlePage}>Criar Conta</Text>
        </View>

        <TextInput
          style={RStyle.InputStyle}
          onChangeText={handleInputChange}
          maxLength={64}
          value={userData.nome}
          placeholder="Nome"
          placeholderTextColor="#A6A6A6"
        />
        <TextInput
          style={RStyle.InputStyle}
          onChangeText={handleInputChange}
          maxLength={64}
          value={userData.email}
          placeholder="Email"
          placeholderTextColor="#A6A6A6"
        />
        <TextInput
          style={RStyle.InputStyle}
          onChangeText={handleInputChange}
          maxLength={64}
          value={userData.telefone}
          placeholder="Telefone"
          placeholderTextColor="#A6A6A6"
        />

        <TextInput
          style={RStyle.InputStyle}
          onChangeText={handleInputChange}
          secureTextEntry={true}
          value={userData.senha}
          maxLength={64}
          placeholder="Senha"
          placeholderTextColor="#A6A6A6"
        />

        <TextInput
          style={RStyle.InputStyle}
          onChangeText={handleInputChange}
          secureTextEntry={true}
          value={userData.confirmarSenha}
          maxLength={64}
          placeholder="Confirmar Senha"
          placeholderTextColor="#A6A6A6"
        />

        <Button
            title="Cadastrar"
            textColor="white"
            borderRadius={20}
            backgroundColor={"#3C9F9F"}
            width="75%"
            height={50}
            sizeText={18}
            textTransform="uppercase"
            marginTop={55}
            marginBottom={20}
            onPress={() => navigation.navigate('Login')}
        />

        <View style={RStyle.TabButtonContainer}>
          <View style={RStyle.TabButton}>
            <Pressable onPress={() => navigation.navigate('Login')}>
              <Text style={RStyle.TabButtonTitle}>Realizar Login</Text>
            </Pressable>
          </View>
        </View>

      </View>
    </ScrollView>
  )
}

export default Register;