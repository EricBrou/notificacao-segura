import * as React from "react"
import Svg, { Path } from "react-native-svg"
const SvgComponent = (props) => (
  <Svg
    xmlns="http://www.w3.org/2000/svg"
    width={27}
    height={27}
    fill="none"
    {...props}
  >
    <Path
      fill="#3C9F9F"
      d="m26.05 2.859-1.583-1.583a2.052 2.052 0 0 0-2.919 0l-7.28 7.281v4.501h4.502l7.28-7.28a2.051 2.051 0 0 0 0-2.919Zm-8.231 8.065H16.4V9.506l4.75-4.746 1.419 1.418-4.751 4.746Z"
    />
    <Path
      fill="#3C9F9F"
      d="M22.529 24.072H3.254V4.797h8.26V2.044h-8.26A2.762 2.762 0 0 0 .5 4.797v19.275a2.762 2.762 0 0 0 2.754 2.754h19.275a2.762 2.762 0 0 0 2.753-2.754v-8.26H22.53v8.26Z"
    />
  </Svg>
)
export default SvgComponent
