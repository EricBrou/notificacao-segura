import * as React from "react"
import Svg, { Path } from "react-native-svg"
const SvgComponent = (props) => (
  <Svg
    xmlns="http://www.w3.org/2000/svg"
    width={27}
    height={27}
    fill="none"
    {...props}
  >
    <Path
      fill="#DC3131"
      d="M7.167 3.267a2.6 2.6 0 0 1 2.6-2.6h7.8a2.6 2.6 0 0 1 2.6 2.6v2.6h5.2a1.3 1.3 0 1 1 0 2.6h-1.39L22.85 24.25a2.6 2.6 0 0 1-2.594 2.416H7.076a2.6 2.6 0 0 1-2.594-2.416L3.358 8.467H1.967a1.3 1.3 0 0 1 0-2.6h5.2v-2.6Zm2.6 2.6h7.8v-2.6h-7.8v2.6Zm-3.804 2.6 1.114 15.6h13.18l1.115-15.6H5.962Zm5.104 2.6a1.3 1.3 0 0 1 1.3 1.3v7.8a1.3 1.3 0 1 1-2.6 0v-7.8a1.3 1.3 0 0 1 1.3-1.3Zm5.2 0a1.3 1.3 0 0 1 1.3 1.3v7.8a1.3 1.3 0 1 1-2.6 0v-7.8a1.3 1.3 0 0 1 1.3-1.3Z"
    />
  </Svg>
)
export default SvgComponent
