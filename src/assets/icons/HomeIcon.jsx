import * as React from "react"
import Svg, { Path } from "react-native-svg"
const SvgComponent = (props) => (
  <Svg
    xmlns="http://www.w3.org/2000/svg"
    width={45}
    height={45}
    fill="none"
    {...props}
  >
    <Path
      fill="#A6A6A6"
      d="M22.5 0A22.776 22.776 0 0 0 0 22.5 22.776 22.776 0 0 0 22.5 45 22.776 22.776 0 0 0 45 22.5 22.776 22.776 0 0 0 22.5 0Zm12.857 24.107h-11.25v11.25h-3.214v-11.25H9.643v-3.214h11.25V9.643h3.214v11.25h11.25v3.214Z"
    />
  </Svg>
)
export default SvgComponent
