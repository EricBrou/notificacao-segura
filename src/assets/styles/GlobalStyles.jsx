import { StyleSheet } from 'react-native';

const GlobalStyles = StyleSheet.create({

    Container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white',
    },

    DefaultImages: {
        width: 250,
        height: 250,
    },

    ContainerScrollView: {
        flexGrow: 1,
        justifyContent: 'center',
    },
  
});

export default GlobalStyles;