import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Login from '../pages/registration/Login';
import Register from '../pages/registration/Register';
import Start from '../pages/registration/Start';
import TabNavigation from './TabNavigation';
import { StatusBar } from 'expo-status-bar';

const stack = createNativeStackNavigator();

function StackNavigation() {
  return (
    <NavigationContainer>
      <StatusBar />
        <stack.Navigator 
          screenOptions={{
          headerTitle: 'Notificação Segura',
          headerTitleAlign: 'center', // Alinha o texto do header no centro
          headerStyle: {
            backgroundColor: '#3C9F9F', // Muda a cor do header
          },
          headerTintColor: '#fff', // Muda a cor do texto do header
          headerTitleStyle: {
            fontWeight: 'bold',
            fontFamily: 'Roboto',
            fontSize: 23,
          },
        }}>

          <stack.Screen 
            name='Start' 
            component={Start} 
            options={{ headerShown: false }} // Remove o Header padrão do navigation
          /> 

          <stack.Screen
            name='Login' 
            component={Login}
            options={{headerBackVisible: false}} // Remove a seta de "Voltar página" do header
          />

          <stack.Screen 
            name='Register' 
            component={Register}
            options={{headerBackVisible: false}} 
          />

          <stack.Screen // Acessando o app após o Login e apresentando a Tab Bar
            name='HomeTabNav' 
            component={TabNavigation}
            options={{headerBackVisible: false}} 
          />

        </stack.Navigator>
    </NavigationContainer>
  )
}

export default StackNavigation;