import * as React from 'react';
import StackNavigation from './StackNavigation';

function Routes() {
  return (
    <StackNavigation />
  )
}

export default Routes;