import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { StatusBar } from 'expo-status-bar';
import Home from '../pages/loggedPages/Home';
import Notification from '../pages/loggedPages/CreateEventPages/Notification';

const tab = createBottomTabNavigator();

function TabNavigation() {
  return (
    <>
      <StatusBar />
        <tab.Navigator
          screenOptions={({ route }) => ({

            tabBarIcon: ({ focused, color, size }) => {
              let iconName;

              switch (route.name) {
                case 'Home': {
                  iconName = focused
                  ? 'home'
                  : 'home-outline';

                  break;
                }
                case 'CreateEvent': {
                  iconName = focused 
                  ? 'add-circle' 
                  : 'add-circle-outline';

                  break;
                }
              }

              return <Ionicons name={iconName} size={32} color={color} />;
            },
            
            headerShown:false,
            labelStyle: { display: 'none' },
            tabBarIconStyle: { width: 30, height: 30, },
            tabBarStyle: { height: 70, backgroundColor: '#EDECEC' },
            tabBarShowLabel: false,
            tabBarActiveTintColor: '#3C9F9F',
            tabBarInactiveTintColor: '#686868',
        })}
        >
            <tab.Screen name="Home" component={Home} />
            <tab.Screen name="CreateEvent" component={Notification} />
        </tab.Navigator>
    </>
  )
}

export default TabNavigation